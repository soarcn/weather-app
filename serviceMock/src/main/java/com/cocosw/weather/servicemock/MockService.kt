package com.cocosw.weather.servicemock

import okhttp3.MediaType
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.mock.BehaviorDelegate
import retrofit2.mock.Calls
import retrofit2.mock.MockRetrofit
import kotlin.reflect.KProperty

abstract class MockService<T>(mockRetrofit: MockRetrofit, private val responses: MockResponseSupplier, clz: Class<T>) {
    private val delegate: BehaviorDelegate<T> = mockRetrofit.create(clz)

    open fun <R : Enum<R>> delegation(cls: Class<R>): T {
        val response = getResponse(cls)
        return if (response is Response<*>)
            delegate.returning(Calls.response(response))
        else
            delegate.returning(Calls.response(response))
    }

    private fun <T : Enum<T>> getResponse(cls: Class<T>): Any? {
        val response = responses.get(cls) as MockResponse
        return if (response.error == null) {
            response.response
        } else {
            Response.error<T>(response.error?.status
                    ?: 400, ResponseBody.create(MediaType.parse("application/json"), response.error?.toBody()
                    ?: ""))
        }
    }

    protected fun toMap(any: Any): Map<String, String> {
        val map = HashMap<String, String>()
        any::class::members.get().forEach {
            if (it is KProperty) {
                map[it.name] = it.getter.call(any).toString()
            }
        }
        return map
    }
}