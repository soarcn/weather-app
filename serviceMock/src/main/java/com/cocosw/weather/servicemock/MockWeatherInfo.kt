package com.cocosw.weather.servicemock

import com.cocosw.weather.service.WeatherInfo

enum class MockWeatherInfo constructor(private val nm: String, override val response: WeatherInfo?, override val error: MockError? = null) : MockResponse {
    SUCCESS("Success", WeatherInfo(name = "city")),
    NOT_FOUND("not found", null, MockErrorBody(404, "{\"code\":\"404\",\"message\":\"city not found\"}"));

    override fun toString(): String {
        return nm
    }
}