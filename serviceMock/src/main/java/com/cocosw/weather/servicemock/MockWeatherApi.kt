package com.cocosw.weather.servicemock

import com.cocosw.weather.service.WeatherApi
import com.cocosw.weather.service.WeatherInfo
import io.reactivex.Single
import retrofit2.mock.MockRetrofit

class MockWeatherApi(mockRetrofit: MockRetrofit, responses: MockResponseSupplier) : MockService<WeatherApi>(mockRetrofit, responses, WeatherApi::class.java),  WeatherApi {
    override fun getWeatherByCity(city: String ): Single<WeatherInfo> {
        return delegation(MockWeatherInfo::class.java).getWeatherByCity(city)
    }
    override fun getWeatherByZipcode(zip: String): Single<WeatherInfo> {
        return delegation(MockZipWeatherInfo::class.java).getWeatherByZipcode(zip)
    }

    override fun getWeatherByGps(lat: Double, lon: Double): Single<WeatherInfo> {
        return delegation(MockGpsWeatherInfo::class.java).getWeatherByGps(lat, lon)
    }

}