package com.cocosw.weather.servicemock

import com.squareup.moshi.Moshi

interface MockResponse {
    val error: MockError?
    val response: Any?
}

open class MockError(open val status: Int, val error: ErrorCode? = null) {
    open fun toBody(): String {
        return "{\"code\":\"$error\"}"
    }
}

class MockErrorBody(override val status: Int, val body: String = "") : MockError(status, null) {
    override fun toBody(): String {
        return body
    }
}

val GENERICERROR: MockError = MockError(400)

val NORESOURCES: MockError = MockError(404)

fun <T> getFromJson(json: String, clz: Class<T>): T {
    val moshi: Moshi = Moshi.Builder().build()
    return moshi.adapter<T>(clz).fromJson(json)
            ?: clz.newInstance()
}

typealias ErrorCode = String