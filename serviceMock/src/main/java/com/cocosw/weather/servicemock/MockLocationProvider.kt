package com.cocosw.weather.servicemock

import com.cocosw.weather.service.LocationProvider
import io.reactivex.Single
import retrofit2.mock.MockRetrofit

class MockLocationProvider(mockRetrofit: MockRetrofit, responses: MockResponseSupplier) :
    MockService<LocationProvider>(mockRetrofit, responses, LocationProvider::class.java),
    LocationProvider {

    override fun getLocation(): Single<Pair<Double, Double>> {
        return delegation(MockLocation::class.java).getLocation()
    }
}