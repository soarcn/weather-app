package com.cocosw.weather.servicemock

import com.cocosw.weather.service.WeatherInfo

enum class MockZipWeatherInfo constructor(
    private val nm: String,
    override val response: WeatherInfo?,
    override val error: MockError? = null
) : MockResponse {
    SUCCESS("Success", WeatherInfo(name = "zip")),
    WRONG_CODE("Failed", null, MockError(422));

    override fun toString(): String {
        return nm
    }
}