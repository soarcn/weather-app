package com.cocosw.weather.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.cocosw.weather.service.WeatherApi
import com.cocosw.weather.service.WeatherInfo
import com.jraska.livedata.test
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext

class LocationWeatherViewModelTest {

    lateinit var vm: LocationWeatherViewModel

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        vm = LocationWeatherViewModel()
        RxAndroidPlugins.setInitMainThreadSchedulerHandler {
            Schedulers.trampoline()
        }
    }


    @Test
    fun `getWeatherByLocation`() {
        StandAloneContext.loadKoinModules(module(override = true) {
            single {
                object : WeatherApi {
                    override fun getWeatherByCity(city: String): Single<WeatherInfo> {
                        return Single.just(WeatherInfo(name = "city"))
                    }

                    override fun getWeatherByZipcode(zip: String): Single<WeatherInfo> {
                        return Single.just(WeatherInfo(name = "zip"))
                    }

                    override fun getWeatherByGps(lat: Double, lon: Double): Single<WeatherInfo> {
                        return Single.just(WeatherInfo(name = "gps"))
                    }
                } as WeatherApi
            }
        })
        val location = 0.0 to 0.0
        vm.submit(location)
        vm.response.test().awaitValue().assertHasValue().assertValue { it.name == "gps" }
    }

}