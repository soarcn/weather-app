package com.cocosw.weather.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.cocosw.weather.base.Status
import com.cocosw.weather.service.WeatherApi
import com.cocosw.weather.service.WeatherInfo
import com.google.common.truth.Truth.assertThat
import com.jraska.livedata.test
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext

class SearchWeatherViewModelTest {

    lateinit var vm: SearchWeatherViewModel

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        vm = SearchWeatherViewModel()
        RxAndroidPlugins.setInitMainThreadSchedulerHandler {
            Schedulers.trampoline()
        }

    }

    @Test
    fun `isNumeric`() {
        assertThat(vm.isNumeric("12334456")).isTrue()
        assertThat(vm.isNumeric("000")).isTrue()
        assertThat(vm.isNumeric("")).isFalse()
        assertThat(vm.isNumeric("00a")).isFalse()
        assertThat(vm.isNumeric("abc")).isFalse()
        assertThat(vm.isNumeric(" ")).isFalse()
    }

    @Test
    fun `getWeatherByCityOrPostcode`() {
        StandAloneContext.loadKoinModules(module(override = true) {
            single {
                object : WeatherApi {
                    override fun getWeatherByCity(city: String): Single<WeatherInfo> {
                        return Single.just(WeatherInfo(name = "city"))
                    }

                    override fun getWeatherByZipcode(zip: String): Single<WeatherInfo> {
                        return Single.just(WeatherInfo(name = "zip"))
                    }

                    override fun getWeatherByGps(lat: Double, lon: Double): Single<WeatherInfo> {
                        return Single.just(WeatherInfo(name = "gps"))
                    }
                } as WeatherApi
            }
        })
        vm.submit("chengdu")
        vm.response.test().awaitValue().assertHasValue().assertValue { it.name == "city" }
        vm.submit("000")
        vm.response.test().awaitValue().assertHasValue().assertValue { it.name == "zip" }
    }

    @Test
    fun `failToGetWeather`() {
        StandAloneContext.loadKoinModules(module(override = true) {
            single {
                object : WeatherApi {
                    override fun getWeatherByCity(city: String): Single<WeatherInfo> {
                        return Single.error(IllegalAccessException())
                    }

                    override fun getWeatherByZipcode(zip: String): Single<WeatherInfo> {
                        return Single.error(IllegalAccessException())
                    }

                    override fun getWeatherByGps(lat: Double, lon: Double): Single<WeatherInfo> {
                        return Single.error(IllegalAccessException())
                    }
                } as WeatherApi
            }
        })
        vm.submit("chengdu")
        vm.state.test().awaitNextValue().assertValue(Status.ERROR)
        vm.response.test().assertNoValue()
        assertThat(vm.error).isInstanceOf(IllegalAccessException::class.java)
        vm = SearchWeatherViewModel()
        vm.submit("000")
        vm.state.test().awaitNextValue().assertValue(Status.ERROR)
        vm.response.test().assertNoValue()
        assertThat(vm.error).isInstanceOf(IllegalAccessException::class.java)
    }
}