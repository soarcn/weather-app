package com.cocosw.weather.base

import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.exceptions.CompositeException
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import retrofit2.Response

abstract class StateViewModel<T> : BaseStateViewModel(), SingleObserver<T> {

    override fun onError(e: Throwable) {
        if (e is CompositeException)
            error(e.exceptions[0])
        else
            error(e)
    }

    override fun onSuccess(t: T) {
        complete()
        onNext(t)
    }

    abstract fun onNext(t: T)

    override operator fun invoke() {
        observable().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(this)
    }

    override fun onSubscribe(d: Disposable) {

    }

    abstract fun observable(): Single<T>


    protected fun proceed(response: Response<Void>): Response<Void> {
        if (response.isSuccessful)
            return response
        else {
            throw HttpException(response)
        }
    }
}


