package com.cocosw.weather.base

import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

abstract class CompletableStateViewModel<T> : BaseStateViewModel(), CompletableObserver {

    override fun onError(e: Throwable) {
        error(e)
    }

    override fun onComplete() {
        onNext()
        complete()
    }

    abstract fun onNext()

    override fun invoke() {
        observable().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(this)
    }

    override fun onSubscribe(d: Disposable) {

    }

    abstract fun observable(): Completable
}


