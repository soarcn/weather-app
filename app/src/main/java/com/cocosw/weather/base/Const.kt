package com.cocosw.weather.base

object ARGUMENT {
    const val GENERIC = "_argument"
}

object CONST {
    const val HEADER_AUTHORIZATION = "Authorization"
    const val HEADER_AUTHORIZATION_BODY = "Bearer "
}

object NAME {
    const val SEARCHHISTORY = "search_history"
    const val IS_MOCK_MODE = "IsMockMode"
    const val API = "Api"
    const val DEFAULT = "default"
    const val FRAMELAYOUT = "frame_layout"
    const val IS_INSTRUMENT_TEST = "IsInstrumentationTest"

    const val WEATHERINFO = "weather_info"
}

object REQUESTCODE {
}
