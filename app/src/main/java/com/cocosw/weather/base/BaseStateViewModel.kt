package com.cocosw.weather.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.squareup.moshi.Moshi
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

/**
 * Base class of state viewmodel
 * State viewmodel is a combination of ViewModel/LiveData/RxJava
 */
abstract class BaseStateViewModel : ViewModel(), KoinComponent {
    private val moshi by inject<Moshi>()
    lateinit var error: Throwable
    val state = MutableLiveData<Status>()

    internal fun error(e: Throwable) {
        error = e
        state.value = Status.ERROR
    }

    internal fun complete() {
        state.value = Status.SUCCESS
    }

    protected fun go() {
        if (Status.LOADING != state.value) {
            state.value = Status.LOADING
            invoke()
        }
    }

    abstract fun invoke()
}