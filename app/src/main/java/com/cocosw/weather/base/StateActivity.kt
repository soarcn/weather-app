package com.cocosw.weather.base

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ProgressBar
import androidx.annotation.VisibleForTesting
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.blankj.utilcode.util.KeyboardUtils
import com.cocosw.weather.R
import com.cocosw.weather.base.NAME.FRAMELAYOUT
import com.cocosw.weather.service.ApiError
import com.google.android.material.snackbar.Snackbar
import com.squareup.moshi.Moshi
import org.koin.android.ext.android.inject
import retrofit2.HttpException
import java.util.concurrent.atomic.AtomicBoolean


abstract class StateActivity : AppCompatActivity() {

    private val layout: Int by inject(FRAMELAYOUT)
    lateinit var progressBar: ProgressBar
    @VisibleForTesting
    var loading = AtomicBoolean(false)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout)
        progressBar = findViewById(R.id.progress)
    }

    fun changeFragment(clz: Class<out androidx.fragment.app.Fragment>, tag: String? = null, args: Bundle? = null) {
        val fragment = androidx.fragment.app.Fragment.instantiate(this, clz.name)
        args?.let { fragment.arguments = it }
        supportFragmentManager.beginTransaction()
                .setTransition(androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.main_content, fragment, tag).commit()
    }

    override fun onDestroy() {
        super.onDestroy()
        KeyboardUtils.fixSoftInputLeaks(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        getVisibleFragment()?.onActivityResult(requestCode, resultCode, data)
    }

    private fun getVisibleFragment(): androidx.fragment.app.Fragment? {
        val fragments = supportFragmentManager.fragments
        for (fragment in fragments) {
            if (fragment != null && fragment.isVisible)
                return fragment
        }
        return null
    }

    override fun onBackPressed() {
        getVisibleFragment()?.apply {
            if (this is BaseFragment) {
                if (!this.onBackPressed()) {
                    super.onBackPressed()
                }
            }
        }
    }
}

fun BaseFragment.bindVM(vm: BaseStateViewModel, spinner: View? = null) {
    vm.state.observe(this.viewLifecycleOwner, Observer {
        when (it) {
            Status.LOADING -> {
                if (activity is StateActivity) {
                    (activity as StateActivity).loading.set(true)
                }
                spinner?.visibility = VISIBLE
                if (activity is StateActivity && spinner == null) {
                    (activity as StateActivity).progressBar.visibility = VISIBLE
                }
            }
            else -> {
                if (it == Status.ERROR) handleError(vm)
                spinner?.visibility = GONE
                if (activity is StateActivity) {
                    (activity as StateActivity).loading.set(false)
                    (activity as StateActivity).progressBar.visibility = GONE
                }
            }
        }
    })
}

fun BaseFragment.handleError(vm: BaseStateViewModel) {
    try {

        val handled = when {
            vm.error is HttpException -> {
                val moshi = Moshi.Builder().build().adapter(ApiError::class.java)
                val error = moshi.fromJson((vm.error as HttpException).response().errorBody()?.string()
                        ?: "") ?: ApiError(400, "")
                this.onVoltError(error)
            }
            else -> this.onError(vm.error)
        }
        if (!handled) {
            Snackbar.make(this.activity?.findViewById(R.id.main_content)!!, vm.error.message.toString(), Snackbar.LENGTH_LONG).show()
        }
        vm.state.postValue(null)
    } catch (e: Exception) {
        Snackbar.make(this.activity?.findViewById(R.id.main_content)!!, vm.error.message.toString(), Snackbar.LENGTH_LONG).show()
    }
}