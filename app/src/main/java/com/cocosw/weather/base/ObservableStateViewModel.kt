package com.cocosw.weather.base

import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

abstract class ObservableStateViewModel<T> : BaseStateViewModel(), Observer<T> {

    override fun onError(e: Throwable) {
        error(e)
    }

    override fun onComplete() {
        complete()
    }

    override fun invoke() {
        observable().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(this)
    }

    override fun onSubscribe(d: Disposable) {

    }

    abstract fun observable(): Observable<T>
}


