package com.cocosw.weather.base

import android.os.Bundle
import android.os.Handler
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.blankj.utilcode.util.KeyboardUtils
import com.cocosw.weather.R
import com.cocosw.weather.service.ApiError
import com.google.android.material.snackbar.Snackbar
import org.koin.standalone.KoinComponent
import kotlin.reflect.KClass


abstract class BaseFragment : androidx.fragment.app.Fragment(), KoinComponent {

    private lateinit var act: AppCompatActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity as AppCompatActivity
    }

    /**
     * Clears the BackStack and displays the instance of the [clz].
     */
    protected fun jump(clz: KClass<out androidx.fragment.app.Fragment>, args: Bundle? = null): androidx.fragment.app.Fragment {
        val fragment = instantiate(act, clz.java.name)
        args?.let { fragment.arguments = it }
        act.supportFragmentManager.beginTransaction()
                .setTransition(androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.main_content, fragment).commit()
        return fragment
    }

    /**
     * Puts an instance of the [clz] on top of the BackStack.
     */
    protected fun start(clz: KClass<out androidx.fragment.app.Fragment>, args: Bundle? = null): androidx.fragment.app.Fragment {
        val fragment = instantiate(act, clz.java.name)
        args?.let { fragment.arguments = it }
        act.supportFragmentManager.beginTransaction()
                .setTransition(androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.main_content, fragment)
                .addToBackStack(fragment.javaClass.name)
                .commit()
        return fragment
    }

    protected fun setupToolbar(toolbar: Toolbar) {
        toolbar.setNavigationOnClickListener {
            backPreviousPage()
        }
    }

    protected fun backPreviousPage() {
        act.supportFragmentManager.popBackStack()
    }

    protected fun backHome() {
        act.supportFragmentManager.popBackStack(null, androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    override fun onResume() {
        super.onResume()
       // if (keyboard.get()) {
        Handler().post{
            if (act.currentFocus !is EditText) {
                KeyboardUtils.hideSoftInput(activity)
            } else {
                KeyboardUtils.showSoftInput(activity)
            }
        }
       // }
    }

    override fun onStop() {
        super.onStop()
      //  if (!keyboard.get()) {
            KeyboardUtils.hideSoftInput(activity)
      //  }
    }

    open fun onError(error: Throwable): Boolean {
        return false
    }

    open fun onVoltError(error: ApiError): Boolean {
        Snackbar.make(act.findViewById(R.id.main_content)!!, error.message
            ?: getText(R.string.generic_error), Snackbar.LENGTH_LONG
        ).show()
        return true
    }

    open fun onBackPressed(): Boolean {
        return false
    }
}