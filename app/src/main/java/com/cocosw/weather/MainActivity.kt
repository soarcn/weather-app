package com.cocosw.weather

import android.os.Bundle
import com.cocosw.weather.base.StateActivity
import com.cocosw.weather.ui.HomeFragment

class MainActivity : StateActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            changeFragment(HomeFragment::class.java)
        }
    }
}
