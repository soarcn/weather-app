package com.cocosw.weather

import android.app.Application
import com.blankj.utilcode.util.Utils
import com.jakewharton.threetenabp.AndroidThreeTen
import org.koin.android.ext.android.startKoin
import org.koin.standalone.KoinComponent

class App : Application(), KoinComponent {

    override fun onCreate() {
        super.onCreate()
        Injector.init(this)
        Utils.init(this)
        AndroidThreeTen.init(this)
        startKoin(this,Injector.module())
    }
}
