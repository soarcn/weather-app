package com.cocosw.weather


import android.app.Application
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.cocosw.weather.base.NAME
import com.cocosw.weather.base.NAME.API
import com.cocosw.weather.base.NAME.DEFAULT
import com.cocosw.weather.base.NAME.FRAMELAYOUT
import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.jakewharton.byteunits.DecimalByteUnit
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.*
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.Module
import org.koin.dsl.module.module
import org.threeten.bp.Clock
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory

/**
 * General dependencies
 */

// Koin module
val appModule: Module = module {
    single(FRAMELAYOUT) { R.layout.main_activity }
    single { provideSharedPreferences(this.androidApplication()) }
    single { provideMoshi() }
    single { provideClock() }
    single { provideRxSharedPreferences(get()) }
    single(NAME.IS_INSTRUMENT_TEST) { false }
}


internal fun provideSharedPreferences(app: Application): SharedPreferences {
    return app.getSharedPreferences("coco",MODE_PRIVATE)
}

internal fun provideRxSharedPreferences(prefs: SharedPreferences): RxSharedPreferences {
    return RxSharedPreferences.create(prefs)
}

internal fun provideMoshi(): Moshi {
    return Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()
}

internal fun provideClock(): Clock {
    return Clock.systemDefaultZone()
}

/**
 * Api dependencies
 */

val apiModule: Module = module {
    single { provideBaseUrl() }
    single(API) { provideApiClient(get(DEFAULT)) }
    single { provideRetrofit(get(), get(API), get()) }
}

internal fun provideBaseUrl(): HttpUrl {
    return PRODUCTION_API_URL!!
}

internal fun provideApiClient(client: OkHttpClient): OkHttpClient {
    return createApiClient(client).build()
}

internal fun provideRetrofit(baseUrl: HttpUrl, client: OkHttpClient,
                             moshi: Moshi): Retrofit {
    return Retrofit.Builder() //
        .client(client) //
        .baseUrl(baseUrl) //
        .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient()) //
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create()) //
        .build()
}

val PRODUCTION_API_URL = HttpUrl.parse("http://api.openweathermap.org/data/2.5/")

fun createApiClient(client: OkHttpClient): OkHttpClient.Builder {
    return client.newBuilder()
        .sslSocketFactory(getDefaultSSLSocketFactory())
        .connectTimeout(1, TimeUnit.MINUTES)
        .readTimeout(1, TimeUnit.MINUTES)
        .writeTimeout(1, TimeUnit.MINUTES)
        .addInterceptor(AppIdInterceptor())
}

private fun getDefaultSSLSocketFactory(): SSLSocketFactory {
    val sslContext = SSLContext.getInstance("TLS")
    sslContext.init(null, null, null)
    return sslContext.socketFactory
}


/**
 * Network dependencies
 */

val networkModule: Module = module {
    single(DEFAULT) { provideOkHttpClient(this.androidApplication()) }
}

private fun provideOkHttpClient(app: Application): OkHttpClient {
    return createOkHttpClient(app).build()
}

val DISK_CACHE_SIZE = DecimalByteUnit.MEGABYTES.toBytes(50)

fun createOkHttpClient(app: Application): OkHttpClient.Builder {
    // Install an HTTP cache in the application cache directory.
    val cacheDir = File(app.cacheDir, "http")
    val cache = Cache(cacheDir, DISK_CACHE_SIZE)

    return OkHttpClient.Builder()
        .cache(cache)
}

private class AppIdInterceptor:Interceptor {
    private val mApiKey = "95d190a434083879a6398aafd54d9e73"
    override fun intercept(chain: Interceptor.Chain): Response {
        val url = chain.request().url()
            .newBuilder()
            .addQueryParameter("appId", mApiKey)
            .build()
        val request = chain.request().newBuilder().url(url).build()
        return chain.proceed(request)
    }
}
