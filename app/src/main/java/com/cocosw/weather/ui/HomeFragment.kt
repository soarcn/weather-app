package com.cocosw.weather.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.blankj.utilcode.util.KeyboardUtils
import com.cocosw.weather.R
import com.cocosw.weather.base.BaseFragment
import com.cocosw.weather.base.NAME
import com.cocosw.weather.service.LocationProvider
import com.f2prateek.rx.preferences2.Preference
import com.mancj.materialsearchbar.MaterialSearchBar
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.home_fragment.*
import org.koin.core.parameter.parametersOf
import org.koin.standalone.inject

class HomeFragment : BaseFragment(), MaterialSearchBar.OnSearchActionListener {

    private var weatherManager: WeatherManager = WeatherManager(this)
    private val locationProvider by inject<LocationProvider> { parametersOf(this) }
    private val searchHistory: Preference<Set<String>> by inject(NAME.SEARCHHISTORY)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        weatherManager.weather.observe(viewLifecycleOwner, Observer {
            result.text = it.toString()
        })
        weatherManager.bindVM(this)
        searchBar.setOnSearchActionListener(this)
        if (searchHistory.isSet) {
            searchBar.lastSuggestions = ArrayList(searchHistory.get())
        }
        fab.setOnClickListener {
            locationProvider.getLocation().subscribeBy(
                onSuccess = {
                    weatherManager.searchByLocation(it)
                },
                onError = {
                    this@HomeFragment.onError(it)
                }
            )
        }
    }

    override fun onButtonClicked(buttonCode: Int) {

    }

    override fun onSearchStateChanged(enabled: Boolean) {

    }

    override fun onSearchConfirmed(text: CharSequence) {
        if (text.isNotBlank()) {
            KeyboardUtils.hideSoftInput(searchBar)
            weatherManager.search(text.toString())
        }
    }

    override fun onDestroyView() {
        searchHistory.set(HashSet(searchBar.lastSuggestions as List<String>))
        super.onDestroyView()
    }
}