package com.cocosw.weather.ui

import androidx.fragment.app.Fragment
import com.cocosw.weather.base.NAME
import com.cocosw.weather.service.LocationProvider
import com.cocosw.weather.service.WeatherApi
import com.cocosw.weather.service.WeatherInfo
import com.f2prateek.rx.preferences2.Preference
import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit

val weatherModule = module {
    viewModel { SearchWeatherViewModel() }
    viewModel { LocationWeatherViewModel() }
    single { provideApiService(get()) }
    factory { (fragment: Fragment) -> provideLocationProvider(fragment) as LocationProvider }
    single(NAME.WEATHERINFO) { provideStore(get(), get()) }
    single(NAME.SEARCHHISTORY) { provideHistory(get()) }

}

fun provideHistory(preferences: RxSharedPreferences): Preference<Set<String>> {
    return preferences.getStringSet(NAME.SEARCHHISTORY)
}

fun provideStore(preferences: RxSharedPreferences, moshi: Moshi): Preference<WeatherInfo> {
    return preferences.getObject(NAME.WEATHERINFO, WeatherInfo(), WeatherAdapter(moshi))
}

private fun provideApiService(retrofit: Retrofit): WeatherApi {
    return retrofit.create(WeatherApi::class.java)
}

private fun provideLocationProvider(fragment: Fragment): LocationProvider {
    return LocusLocationProvider(fragment)
}

private class WeatherAdapter(moshi: Moshi) : Preference.Converter<WeatherInfo> {
    private var serializer: JsonAdapter<WeatherInfo>? = moshi.adapter(WeatherInfo::class.java)

    override fun deserialize(serialized: String): WeatherInfo {
        return serializer?.fromJson(serialized) ?: WeatherInfo()
    }

    override fun serialize(value: WeatherInfo): String {
        return serializer?.toJson(value) ?: ""
    }
}