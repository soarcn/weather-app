package com.cocosw.weather.ui

import androidx.annotation.VisibleForTesting
import com.cocosw.weather.base.SingleLiveEvent
import com.cocosw.weather.base.StateViewModel
import com.cocosw.weather.service.WeatherApi
import com.cocosw.weather.service.WeatherInfo
import io.reactivex.Single
import org.koin.standalone.inject

class SearchWeatherViewModel : StateViewModel<WeatherInfo>() {

    private val service: WeatherApi by inject()
    lateinit var q: String
    val response: SingleLiveEvent<WeatherInfo> = SingleLiveEvent()

    override fun onNext(t: WeatherInfo) {
        response.value = t
    }

    override fun observable(): Single<WeatherInfo> {
        return if (isNumeric(q)) service.getWeatherByZipcode(q) else service.getWeatherByCity(q)
    }

    @VisibleForTesting
    internal fun isNumeric(q: String): Boolean {
        return q.matches(Regex("[0-9]+"))
    }

    fun submit(query: String) {
        this.q = query
        go()
    }
}