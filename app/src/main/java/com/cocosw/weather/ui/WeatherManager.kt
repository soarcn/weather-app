package com.cocosw.weather.ui

import androidx.lifecycle.*
import com.cocosw.weather.base.BaseFragment
import com.cocosw.weather.base.NAME
import com.cocosw.weather.base.SingleLiveEvent
import com.cocosw.weather.base.bindVM
import com.cocosw.weather.service.WeatherInfo
import com.f2prateek.rx.preferences2.Preference
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class WeatherManager(lifecycleOwner: LifecycleOwner) : LifecycleObserver, KoinComponent {

    private val vm by lifecycleOwner.viewModel<SearchWeatherViewModel>()
    private val locationViewModel by lifecycleOwner.viewModel<LocationWeatherViewModel>()
    private val store: Preference<WeatherInfo> by inject(NAME.WEATHERINFO)

    private val storeLiveData = SingleLiveEvent<WeatherInfo>()

    /**
     * A weatherInfo livedate object, result comes from service
     */
    internal val weather = MediatorLiveData<WeatherInfo>()

    init {
        lifecycleOwner.lifecycle.addObserver(this)
    }

    private fun addSource(vararg sources: LiveData<WeatherInfo>): LiveData<WeatherInfo> {
        sources.forEach {
            weather.addSource(it) { weatherInfo ->
                weather.value = weatherInfo
            }
        }
        return weather
    }

    /**
     * Bind the instance to a BaseFragment, so the view-models state changes can be observed by the fragment.
     *
     */
    internal fun bindVM(fragment: BaseFragment) {
        fragment.bindVM(vm)
        fragment.bindVM(locationViewModel)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    internal fun addSource() {
        addSource(storeLiveData, vm.response, locationViewModel.response)
        if (store.isSet) storeLiveData.value = store.get()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    internal fun saveWeather() {
        weather.value?.apply {
            store.set(this)
        }
    }

    /**
     * Query weather info by providing location
     */
    fun searchByLocation(location: Pair<Double, Double>) {
        locationViewModel.submit(location)
    }

    /**
     * Query weather info by providing a city name or zip code
     */
    fun search(q: String) {
        vm.submit(q)
    }
}