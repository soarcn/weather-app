package com.cocosw.weather.ui

import com.cocosw.weather.base.SingleLiveEvent
import com.cocosw.weather.base.StateViewModel
import com.cocosw.weather.service.WeatherApi
import com.cocosw.weather.service.WeatherInfo
import io.reactivex.Single
import org.koin.standalone.inject


class LocationWeatherViewModel : StateViewModel<WeatherInfo>() {

    private val service: WeatherApi by inject()
    val response: SingleLiveEvent<WeatherInfo> = SingleLiveEvent()
    lateinit var location: Pair<Double, Double>

    override fun onNext(t: WeatherInfo) {
        response.value = t
    }

    override fun observable(): Single<WeatherInfo> {
        return service.getWeatherByGps(location.first, location.second)
    }

    fun submit(location: Pair<Double, Double>) {
        this.location = location
        go()
    }
}