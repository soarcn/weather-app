package com.cocosw.weather.ui

import androidx.fragment.app.Fragment
import com.birjuvachhani.locus.Locus
import com.cocosw.weather.service.LocationProvider
import io.reactivex.Single

class LocusLocationProvider(private val fragment: Fragment) : LocationProvider {

    private val locus = Locus()

    override fun getLocation(): Single<Pair<Double, Double>> {
        return Single.create {
            locus.getCurrentLocation(fragment) {
                it.onSuccess(this.latitude to this.longitude)
            } failure {
                it.onError(this)
            }
        }
    }
}