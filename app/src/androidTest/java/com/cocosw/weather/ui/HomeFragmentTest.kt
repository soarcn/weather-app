package com.cocosw.weather.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.pressImeActionButton
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.cocosw.weather.R
import com.cocosw.weather.servicemock.MockResponseSupplier
import com.cocosw.weather.servicemock.MockWeatherInfo
import com.cocosw.weather.testing.SingleFragmentActivity
import com.cocosw.weather.utils.BetterActivityTestRule
import com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions.assertDisplayed
import com.schibsted.spain.barista.interaction.BaristaClickInteractions.clickOn
import com.schibsted.spain.barista.interaction.BaristaEditTextInteractions.writeTo
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.standalone.inject
import org.koin.test.KoinTest

class HomeFragmentTest : KoinTest {

    @Rule
    @JvmField
    val activityRule = BetterActivityTestRule(SingleFragmentActivity::class.java)
    private val mock: MockResponseSupplier by inject()

    @Before
    fun setup() {
        activityRule.activity.setFragment(HomeFragment())
    }

    @Test
    fun search_weather_by_zip() {
        clickOn(R.id.searchBar)
        writeTo(R.id.mt_editText, "000")
        onView(withId(R.id.mt_editText)).perform(pressImeActionButton())

        assertDisplayed("zip 0.0")
    }

    @Test
    fun search_weather_by_city() {
        clickOn(R.id.searchBar)
        writeTo(R.id.mt_editText, "sydney")
        onView(withId(R.id.mt_editText)).perform(pressImeActionButton())

        assertDisplayed("city 0.0")
    }

    @Test
    fun search_weather_by_gps() {
        clickOn(R.id.fab)
        assertDisplayed("GPS 0.0")
    }
 
    @Test
    fun city_not_found() {
        mock.set(MockWeatherInfo.NOT_FOUND)
        clickOn(R.id.searchBar)
        writeTo(R.id.mt_editText, "sydney")
        onView(withId(R.id.mt_editText)).perform(pressImeActionButton())

        onView(withId(com.google.android.material.R.id.snackbar_text))
            .check(matches(withText("city not found")))
    }
}