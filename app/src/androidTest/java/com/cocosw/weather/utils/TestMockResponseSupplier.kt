package com.cocosw.weather.utils

import com.cocosw.weather.servicemock.MockResponseSupplier


class TestMockResponseSupplier : MockResponseSupplier {

    val map: HashMap<String, String> = HashMap()

    override fun <T : Enum<T>> get(cls: Class<T>): T {
        val value = map[cls.canonicalName!!]
        return if (value != null) {
            java.lang.Enum.valueOf(cls, value)
        } else cls.enumConstants[0]
    }

    override fun set(value: Enum<*>) {
        map[value::class.java.canonicalName!!] = value.name
    }

    companion object {

        private val instance = TestMockResponseSupplier()

        fun getInstance(): TestMockResponseSupplier {
            return instance
        }

        fun reset() {
            instance.map.clear()
        }
    }
}