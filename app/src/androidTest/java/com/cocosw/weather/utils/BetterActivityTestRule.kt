package com.cocosw.weather.utils

import android.app.Activity
import android.content.pm.PackageManager
import android.view.WindowManager
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.UiDevice
import com.blankj.utilcode.util.KeyboardUtils
import com.cocosw.weather.base.StateActivity
import org.junit.runner.Description
import org.junit.runners.model.Statement

/**
 * Created by TimR.
 */
class BetterActivityTestRule<T : Activity>(activityClass: Class<T>, val beforeAll: (() -> Unit)? = null) : ActivityTestRule<T>(activityClass, true, true) {

    private val ANIMATION_SCALE_PERMISSION = "android.permission.SET_ANIMATION_SCALE"

    private lateinit var launchActivity: Activity
    private lateinit var device: UiDevice


    override fun apply(userTest: Statement, description: Description): Statement {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        return object : Statement() {
            override fun evaluate() {
                device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
                deviceWakeUp()
                beforeAll?.invoke()
                launchActivity = launchActivity(activityIntent)
                installWakeLock()

                grantScalePermission()

                runTest(userTest)
            }
        }
    }

    private lateinit var idling: IdlingResource

    private fun runTest(base: Statement) {
        idling = object : IdlingResource {

            lateinit var resourceCallback: IdlingResource.ResourceCallback

            override fun isIdleNow(): Boolean {
                val idle = if (activity is StateActivity) {
                    (activity as StateActivity).loading.get().not()
                } else
                    true
                if (idle) {
                    resourceCallback.onTransitionToIdle()
                }
                return idle
            }

            override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback) {
                this.resourceCallback = callback
            }

            override fun getName(): String {
                return "LoadingIdle"
            }
        }
        IdlingRegistry.getInstance().register(idling)
        Espresso.closeSoftKeyboard()
        setInTestMode(true)
        try {
            base.evaluate()
        } finally {
            IdlingRegistry.getInstance().unregister(idling)
            setInTestMode(false)
            KeyboardUtils.hideSoftInput(launchActivity)
            Espresso.closeSoftKeyboard()
        }
    }

    //<editor-fold desc="Helper Function">
    private fun setInTestMode(isInTestMode: Boolean) {
        //toggleSoftKeyboard(!isInTestMode)
        toggleAnimations(!isInTestMode)
        setDemoMode(isInTestMode)
        TestMockResponseSupplier.reset()
    }

    private fun toggleAnimations(isEnabled: Boolean) {
        val permStatus = launchActivity.checkCallingOrSelfPermission(ANIMATION_SCALE_PERMISSION)
        if (permStatus == PackageManager.PERMISSION_GRANTED) {
            AnimationType.values().forEach {
                device.executeShellCommand(it.shellCommand + booleanToInt(isEnabled))
            }
        } else {
            throw RuntimeException("You did not declare the permission")
        }
    }

    private fun booleanToInt(isEnabled: Boolean): Int = if (isEnabled) 1 else 0

    private fun deviceWakeUp() {
        device.let {
            if (it.isScreenOn) {
                it.wakeUp()
            }
        }
    }

    /**
     * Bypasses the activity to run on top of the lockscreen
     */
    private fun installWakeLock() {
        this.runOnUiThread {
            launchActivity.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
                    WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)

        }
    }

    /**
     *
     * You should define the following permission in your AndroidManifest.xml
     * <uses-permission android:name="android.permission.SET_ANIMATION_SCALE"/>
     * Be sure you exclude this permission in release builds
     *
     * Either via gradle script or using different AndroidManifest files
     * for different flavors
     *
     */
    private fun grantScalePermission() {
        device.executeShellCommand("pm grant ${launchActivity.packageName} $ANIMATION_SCALE_PERMISSION")
    }

    enum class AnimationType(val shellCommand: String) {
        WINDOW_ANIMATION_SCALE("settings put global window_animation_scale "),
        TRANSITION_ANIMATION_SCALE("settings put global transition_animation_scale "),
        ANIMATOR_DURATION_SCALE("settings put global animator_duration_scale ")
    }
    //</editor-fold>

    //<editor-fold desc="Demo Mode">
    private fun setDemoMode(inDemoMode: Boolean) {
        val currentApiVersion = android.os.Build.VERSION.SDK_INT
        if (currentApiVersion >= android.os.Build.VERSION_CODES.M) {
            device.executeShellCommand("settings put global sysui_demo_allowed" + booleanToInt(inDemoMode))

            val command = "am broadcast -a com.android.systemui.demo -e command" + if (inDemoMode) "enter" else "exit"
            device.executeShellCommand(command)
        }
    }


    enum class WifiLevel(val level: String) {
        LEVEL_1("1"),
        LEVEL_2("2"),
        LEVEL_3("3"),
        LEVEL_4("4")
    }
    //</editor-fold>
}