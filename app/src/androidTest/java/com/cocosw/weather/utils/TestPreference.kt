package com.cocosw.weather.utils

import com.f2prateek.rx.preferences2.Preference
import io.reactivex.Observable
import io.reactivex.functions.Consumer

open class TestPreference<T>(private val default: T) : Preference<T> {

    private var value: T? = null

    override fun isSet(): Boolean {
        return value != null
    }

    override fun key(): String {
        return "test"
    }

    override fun asObservable(): Observable<T> {
        return Observable.just(value)
    }

    override fun asConsumer(): Consumer<in T> {
        return Consumer<T> { set(get()) }
    }

    override fun defaultValue(): T {
        return default
    }

    override fun get(): T {
        return value ?: default
    }

    override fun set(value: T) {
        this.value = value
    }

    override fun delete() {
        value = null
    }

}