package com.cocosw.weather

import android.annotation.SuppressLint
import android.app.Application
import android.app.KeyguardManager
import android.content.Context.KEYGUARD_SERVICE
import android.content.Context.POWER_SERVICE
import android.os.PowerManager
import android.os.PowerManager.*
import androidx.test.runner.AndroidJUnitRunner
import com.blankj.utilcode.util.Utils
import com.jakewharton.threetenabp.AndroidThreeTen
import org.koin.standalone.StandAloneContext
import org.koin.test.KoinTest

class TestRunner : AndroidJUnitRunner(),KoinTest{
    private var wakeLock: PowerManager.WakeLock? = null

    @SuppressLint("MissingPermission")
    override fun onStart() {
        val app = targetContext.applicationContext
        val name = TestRunner::class.java.simpleName
        // Unlock the device so that the tests can input keystrokes.
        val keyguard = app.getSystemService(KEYGUARD_SERVICE) as KeyguardManager
        keyguard.newKeyguardLock(name).disableKeyguard()
        // Wake up the screen.
        val power = app.getSystemService(POWER_SERVICE) as PowerManager
        wakeLock = power.newWakeLock(FULL_WAKE_LOCK or ACQUIRE_CAUSES_WAKEUP or ON_AFTER_RELEASE, name)
        wakeLock?.acquire()
        Utils.init(app)
        AndroidThreeTen.init(app)
        super.onStart()
    }

    override fun callApplicationOnCreate(app: Application?) {
        super.callApplicationOnCreate(app)
        StandAloneContext.loadKoinModules(testModule)
    }

    override fun onDestroy() {
        super.onDestroy()
        wakeLock?.release()
    }

}