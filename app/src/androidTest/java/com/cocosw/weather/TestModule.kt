package com.cocosw.weather


import com.cocosw.weather.base.NAME
import com.cocosw.weather.servicemock.MockResponseSupplier
import com.cocosw.weather.utils.TestMockResponseSupplier
import com.f2prateek.rx.preferences2.Preference
import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.jakewharton.u2020.data.ApiEndpoints
import org.koin.dsl.module.Module
import org.koin.dsl.module.module
import retrofit2.mock.NetworkBehavior
import java.util.concurrent.TimeUnit


val testModule: Module = module(override = true) {
    single { provideBehavior() }
    single(NAME.FRAMELAYOUT) { R.layout.main_activity }
    single(NAME.IS_INSTRUMENT_TEST) { true }
    single { TestMockResponseSupplier.getInstance() as MockResponseSupplier }
    single("ApiEndpoint") { provideTestEndpoint(get()) }
    single(NAME.IS_MOCK_MODE) { true }
}


fun provideTestEndpoint(preferences: RxSharedPreferences): Preference<String> {
    return preferences.getString("debug_endpoint", ApiEndpoints.MOCK_MODE.url)
}


internal fun provideBehavior(): NetworkBehavior {
    val behavior = NetworkBehavior.create()
    behavior.setDelay(0, TimeUnit.SECONDS)
    behavior.setErrorPercent(0)
    behavior.setVariancePercent(0)
    behavior.setFailurePercent(0)
    return behavior
}
