package com.jakewharton.u2020.data

import android.annotation.SuppressLint
import android.content.SharedPreferences
import com.cocosw.weather.servicemock.MockResponseSupplier

internal class SharedPreferencesMockResponseSupplier(private val preferences: SharedPreferences) :
    MockResponseSupplier {

    override fun <T : Enum<T>> get(cls: Class<T>): T {
        val value = preferences.getString(cls.canonicalName, null)
        return if (value != null) {
            java.lang.Enum.valueOf(cls, value)
        } else cls.enumConstants[0]
    }

    @SuppressLint("ApplySharedPref") // Persist to disk because we might kill the process next.
    override fun set(value: Enum<*>) {
        preferences.edit()
                .putString(value::class.java.canonicalName, value.name)
                .commit()
    }
}
