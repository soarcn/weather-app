package com.jakewharton.u2020.data


enum class ApiEndpoints(val apiname: String, val url: String) {
    PROD("PROD", "http://api.openweathermap.org/data/2.5/"),
    MOCK_MODE("Mock Mode", "http://localhost/mock/"),
    CUSTOM("Custom", "");

    override fun toString(): String {
        return apiname
    }

    companion object {

        fun from(endpoint: String): ApiEndpoints {
            for (value in values()) {
                if (value.url == endpoint) {
                    return value
                }
            }
            return CUSTOM
        }

        fun isMockMode(endpoint: String): Boolean {
            return from(endpoint) == MOCK_MODE
        }
    }
}
