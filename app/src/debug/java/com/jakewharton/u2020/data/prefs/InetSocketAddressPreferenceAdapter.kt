package com.jakewharton.u2020.data.prefs

import com.f2prateek.rx.preferences2.Preference
import java.net.InetSocketAddress
import java.net.Proxy
import java.net.Proxy.Type.HTTP

class InetSocketAddressPreferenceAdapter internal constructor() : Preference.Converter<InetSocketAddress> {
    override fun deserialize(value: String): InetSocketAddress {
        val parts = value.split(":".toRegex(), 2).toTypedArray()
        val host = parts[0]
        val port = if (parts.size > 1) Integer.parseInt(parts[1]) else 80
        return InetSocketAddress.createUnresolved(host, port)
    }

    override fun serialize(address: InetSocketAddress): String {
        val host: String? = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            address.hostString
        } else {
            address.hostName
        }
        val port = address.port
        return "$host:$port"
    }

    companion object {
        val INSTANCE = InetSocketAddressPreferenceAdapter()

        fun parse(value: String?): InetSocketAddress? {
            if (value.isNullOrBlank()) {
                return null
            }
            val parts = value.split(":".toRegex(), 2).toTypedArray()
            if (parts.size == 0) {
                return null
            }
            val host = parts[0]
            val port = if (parts.size > 1) Integer.parseInt(parts[1]) else 80
            return InetSocketAddress.createUnresolved(host, port)
        }

        fun createProxy(address: InetSocketAddress?): Proxy? {
            return if (address == null) {
                null
            } else Proxy(HTTP, address)
        }
    }
}
