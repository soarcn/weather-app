package com.jakewharton.u2020.ui.logs

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.DrawableRes
import com.cocosw.weather.R
import com.cocosw.weather.base.LumberYard.Entry
import com.jakewharton.u2020.ui.misc.BindableAdapter
import io.reactivex.functions.Consumer
import java.util.*

internal class LogAdapter(context: Context) : BindableAdapter<Entry>(context), Consumer<Entry> {
    private var logs: MutableList<Entry> = Collections.emptyList()


    fun setLogs(logs: List<Entry>) {
        this.logs = ArrayList(logs)
    }

    override fun accept(entry: Entry) {
        logs.add(entry)
        notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return logs.size
    }

    override fun getItem(i: Int): Entry {
        return logs[i]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    override fun newView(inflater: LayoutInflater, position: Int, container: ViewGroup): View {
        val view = inflater.inflate(R.layout.debug_logs_list_item, container, false)
        val viewHolder = LogItemViewHolder(view)
        view.tag = viewHolder
        return view
    }

    override fun bindView(item: Entry?, position: Int, view: View?) {
        val viewHolder = view?.tag as LogItemViewHolder
        item?.apply { viewHolder.setEntry(item) }
    }

    internal class LogItemViewHolder(private val rootView: View) {
        var levelView: TextView = rootView.findViewById(R.id.debug_log_level)
        var tagView: TextView = rootView.findViewById(R.id.debug_log_tag)
        var messageView: TextView = rootView.findViewById(R.id.debug_log_message)

        fun setEntry(entry: Entry) {
            rootView.setBackgroundResource(backgroundForLevel(entry.level))
            levelView.text = entry.displayLevel()
            tagView.text = entry.tag
            messageView.text = entry.message
        }
    }
}

@DrawableRes
private fun backgroundForLevel(level: Int): Int {
    when (level) {
        Log.VERBOSE, Log.DEBUG -> return R.color.debug_log_accent_debug
        Log.INFO -> return R.color.debug_log_accent_info
        Log.WARN -> return R.color.debug_log_accent_warn
        Log.ERROR, Log.ASSERT -> return R.color.debug_log_accent_error
        else -> return R.color.debug_log_accent_unknown
    }
}
