package com.jakewharton.u2020.ui.debug

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Spinner
import android.widget.SpinnerAdapter
import com.cocosw.weather.R
import com.cocosw.weather.servicemock.MockLocation
import com.cocosw.weather.servicemock.MockResponseSupplier
import com.cocosw.weather.servicemock.MockWeatherInfo
import com.cocosw.weather.servicemock.MockZipWeatherInfo

import com.jakewharton.rxbinding2.widget.RxAdapterView
import com.jakewharton.u2020.ui.misc.EnumAdapter
import org.koin.android.ext.android.inject
import timber.log.Timber

class MockSettingDialog : androidx.fragment.app.DialogFragment() {
    val mockResponseSupplier: MockResponseSupplier by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.Theme_AppCompat_Light_Dialog)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.debug_setting, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureResponseSpinner(view.findViewById(R.id.city), MockWeatherInfo::class.java)
        configureResponseSpinner(view.findViewById(R.id.zip), MockZipWeatherInfo::class.java)
        configureResponseSpinner(view.findViewById(R.id.location), MockLocation::class.java)
    }

    /**
     * Populates a `Spinner` with the values of an `enum` and binds it to the value set
     * in
     * the mock service.
     */
    private fun <T : Enum<T>> configureResponseSpinner(spinner: Spinner,
                                                       responseClass: Class<T>) {
        val adapter = EnumAdapter(requireContext(), responseClass)
        spinner.adapter = adapter
        spinner.setSelection(mockResponseSupplier.get(responseClass).ordinal)

        RxAdapterView.itemSelections<SpinnerAdapter>(spinner)
                .map { adapter.getItem(it) }
                .filter { item -> item !== mockResponseSupplier.get(responseClass) }
                .subscribe { selected ->
                    Timber.d("Setting %s to %s", responseClass.simpleName, selected)
                    selected?.let { mockResponseSupplier.set(it) }
                }
    }

}