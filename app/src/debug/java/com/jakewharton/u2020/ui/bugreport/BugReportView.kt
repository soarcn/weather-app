package com.jakewharton.u2020.ui.bugreport

import android.content.Context
import android.util.AttributeSet
import android.widget.CheckBox
import android.widget.EditText
import android.widget.LinearLayout
import com.cocosw.weather.R

class BugReportView(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {
    lateinit var titleView: EditText
    lateinit var descriptionView: EditText
    lateinit var screenshotView: CheckBox
    lateinit var logsView: CheckBox

    private var listener: ReportDetailsListener? = null

    val report: Report
        get() = Report(titleView.text.toString(),
                descriptionView.text.toString(), screenshotView.isChecked,
                logsView.isChecked)

    interface ReportDetailsListener {
        fun onStateChanged(valid: Boolean)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        titleView = findViewById(R.id.title)
        descriptionView = findViewById(R.id.description)
        screenshotView = findViewById(R.id.screenshot)
        logsView = findViewById(R.id.logs)
        titleView.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                titleView.error = if (titleView.text.isBlank()) "Cannot be empty." else null
            }
        }

        screenshotView.isChecked = true
        logsView.isChecked = true
    }

    fun setBugReportListener(listener: ReportDetailsListener) {
        this.listener = listener
    }

    class Report(val title: String, val description: String, val includeScreenshot: Boolean,
                 val includeLogs: Boolean)
}
