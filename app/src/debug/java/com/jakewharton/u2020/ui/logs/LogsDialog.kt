package com.jakewharton.u2020.ui.logs

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.StrictMode
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.cocosw.weather.base.LumberYard
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.koin.standalone.KoinComponent
import java.io.File


class LogsDialog(context: Context, private val lumberYard: LumberYard) : AlertDialog(context), KoinComponent {
    private val adapter: LogAdapter = LogAdapter(context)

    private lateinit var subscriptions: CompositeDisposable

    init {

        val listView = ListView(context)
        listView.transcriptMode = ListView.TRANSCRIPT_MODE_NORMAL
        listView.adapter = adapter
        listView.post {
            // Select the last row so it will scroll into view...
            listView.setSelection(adapter.count - 1)
        }
        setTitle("Logs")
        setView(listView)
        setButton(DialogInterface.BUTTON_NEGATIVE, "Close") { _, _ ->
            // NO-OP.
        }
        setButton(DialogInterface.BUTTON_POSITIVE, "Share") { _, _ -> share() }
    }

    override fun onStart() {
        super.onStart()

        adapter.setLogs(lumberYard.bufferedLogs())

        subscriptions = CompositeDisposable()
        subscriptions.add(lumberYard.logs() //
                .observeOn(AndroidSchedulers.mainThread()) //
                .subscribe(adapter))
    }

    override fun onStop() {
        super.onStop()
        subscriptions.clear()
    }

    private fun share() {
        lumberYard.save() //
                .subscribeOn(Schedulers.io()) //
                .observeOn(AndroidSchedulers.mainThread()) //
                .subscribe(object : Observer<File> {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onComplete() {
                        // NO-OP.
                    }

                    override fun onError(e: Throwable) {
                        Toast.makeText(context, "Couldn't save the logs for sharing.", Toast.LENGTH_SHORT)
                                .show()
                    }

                    override fun onNext(file: File) {
                        val sendIntent = Intent(Intent.ACTION_SEND)
                        val builder = StrictMode.VmPolicy.Builder()
                        StrictMode.setVmPolicy(builder.build())
                        sendIntent.type = "text/plain"
                        sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file))
                        context.startActivity(sendIntent)
                    }
                })
    }
}
