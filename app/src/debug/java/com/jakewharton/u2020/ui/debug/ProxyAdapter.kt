package com.jakewharton.u2020.ui.debug

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.f2prateek.rx.preferences2.Preference
import com.jakewharton.u2020.ui.misc.BindableAdapter
import java.net.InetSocketAddress

internal class ProxyAdapter(context: Context, private val proxyAddress: Preference<InetSocketAddress>) : BindableAdapter<String>(context) {

    override fun getCount(): Int {
        return 2 /* "None" and "Set" */ + if (proxyAddress.isSet) 1 else 0
    }

    override fun getItem(position: Int): String {
        if (position == 0) {
            return "None"
        }
        return if (position == count - 1) {
            "Set…"
        } else proxyAddress.get().toString()
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun newView(inflater: LayoutInflater, position: Int, container: ViewGroup): View {
        return inflater.inflate(android.R.layout.simple_spinner_item, container, false)
    }

    override fun bindView(item: String?, position: Int, view: View?) {
        val tv = view!!.findViewById<TextView>(android.R.id.text1)
        tv.text = item
    }

    override fun newDropDownView(inflater: LayoutInflater, position: Int, container: ViewGroup): View {
        return inflater.inflate(android.R.layout.simple_spinner_dropdown_item, container, false)
    }

    companion object {
        val NONE = 0
        val PROXY = 1
    }
}
