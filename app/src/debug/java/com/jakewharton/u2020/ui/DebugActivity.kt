package com.jakewharton.u2020.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.blankj.utilcode.util.Utils
import com.cocosw.weather.R

class DebugActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Utils.init(application!!)
        setContentView(R.layout.debug_activity)
    }
}
