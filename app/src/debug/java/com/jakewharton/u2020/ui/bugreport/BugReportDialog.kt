package com.jakewharton.u2020.ui.bugreport

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import com.cocosw.weather.R
import com.jakewharton.u2020.ui.bugreport.BugReportView.Report
import com.jakewharton.u2020.ui.bugreport.BugReportView.ReportDetailsListener

class BugReportDialog(context: Context) : AlertDialog(context), ReportDetailsListener {

    private var listener: ReportListener? = null

    interface ReportListener {
        fun onBugReportSubmit(report: Report)
    }

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.bugreport_view, null) as BugReportView
        view.setBugReportListener(this)

        setTitle("Report a bug")
        setView(view)
        setButton(Dialog.BUTTON_NEGATIVE, "Cancel", null as DialogInterface.OnClickListener?)
        setButton(Dialog.BUTTON_POSITIVE, "Submit") { _, _ ->
            if (listener != null) {
                listener!!.onBugReportSubmit(view.report)
            }
        }
    }

    fun setReportListener(listener: ReportListener) {
        this.listener = listener
    }

    override fun onStart() {
        getButton(Dialog.BUTTON_POSITIVE).isEnabled = false
    }

    override fun onStateChanged(valid: Boolean) {
        getButton(Dialog.BUTTON_POSITIVE).isEnabled = valid
    }
}