package com.jakewharton.u2020.ui.debug

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.View
import com.cocosw.weather.R
import com.cocosw.weather.base.LumberYard
import com.f2prateek.rx.preferences2.Preference
import com.jakewharton.madge.MadgeFrameLayout
import com.jakewharton.scalpel.ScalpelFrameLayout
import com.jakewharton.u2020.ui.bugreport.BugReportLens
import com.mattprecious.telescope.TelescopeLayout
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject


class DebugDrawerLayout(context: Context, attrs: AttributeSet) :
    androidx.drawerlayout.widget.DrawerLayout(context, attrs), KoinComponent {

    private val lumberYard: LumberYard by inject()

    val pixelGridEnabled: Preference<Boolean> by inject("PixelGridEnabled")
    val pixelRatioEnabled: Preference<Boolean> by inject("PixelRatioEnabled")
    val scalpelEnabled: Preference<Boolean> by inject("ScalpelEnabled")
    val scalpelWireframeEnabled: Preference<Boolean> by inject("ScalpelWireframeEnabled")

    override fun onFinishInflate() {
        super.onFinishInflate()
        findViewById<TelescopeLayout>(R.id.telescope_container).setLens(BugReportLens(context as Activity, lumberYard))
        setupMadge()
        setupScalpel()
    }

    private fun setupMadge() {
        pixelGridEnabled.asObservable()
            .subscribe { enabled -> findViewById<MadgeFrameLayout>(R.id.madge_container).setOverlayEnabled(enabled) }
        pixelRatioEnabled.asObservable()
            .subscribe { enabled -> findViewById<MadgeFrameLayout>(R.id.madge_container).setOverlayRatioEnabled(enabled) }
    }

    private fun setupScalpel() {
        scalpelEnabled.asObservable().subscribe { enabled ->
            findViewById<ScalpelFrameLayout>(R.id.debug_content).setLayerInteractionEnabled(enabled)
        }
        scalpelWireframeEnabled.asObservable()
            .subscribe { enabled -> findViewById<ScalpelFrameLayout>(R.id.debug_content).setDrawViews(!enabled) }
    }

}
