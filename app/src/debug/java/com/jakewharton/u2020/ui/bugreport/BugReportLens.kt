package com.jakewharton.u2020.ui.bugreport

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.StrictMode
import android.text.TextUtils
import android.util.DisplayMetrics
import android.widget.Toast
import androidx.core.app.ShareCompat
import com.cocosw.weather.base.LumberYard
import com.mattprecious.telescope.Lens
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.io.File


/**
 * Pops a dialog asking for more information about the bug report and then creates an email with a
 * JIRA-formatted body.
 */
class BugReportLens(private val context: Activity, private val lumberYard: LumberYard) : Lens(), BugReportDialog.ReportListener {

    private var screenshot: File? = null

    override fun onCapture(screenshot: File?) {
        this.screenshot = screenshot

        val dialog = BugReportDialog(context)
        dialog.setReportListener(this)
        dialog.show()
    }

    override fun onBugReportSubmit(report: BugReportView.Report) {
        if (report.includeLogs) {
            lumberYard.save()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : Observer<File> {
                        override fun onSubscribe(d: Disposable) {
                        }

                        override fun onComplete() {
                            // NO-OP.
                        }

                        override fun onError(e: Throwable) {
                            Toast.makeText(context, "Couldn't attach the logs.", Toast.LENGTH_SHORT).show()
                            submitReport(report, null)
                        }

                        override fun onNext(logs: File) {
                            submitReport(report, logs)
                        }
                    })
        } else {
            submitReport(report, null)
        }
    }

    private fun submitReport(report: BugReportView.Report, logs: File?) {
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        val dm = context.resources.displayMetrics
        val densityBucket = getDensityString(dm)

        val intent = ShareCompat.IntentBuilder.from(context)
                .setType("message/rfc822")
                // TODO: .addEmailTo("u2020-bugs@blackhole.io")
                .setSubject(report.title)

        val body = StringBuilder()
        if (TextUtils.isEmpty(report.description)) {
            body.append("{panel:title=Description}\n").append(report.description).append("\n{panel}\n\n")
        }

        body.append("{panel:title=App}\n")
        body.append("{panel}\n\n")

        body.append("{panel:title=Device}\n")
        body.append("Make: ").append(Build.MANUFACTURER).append('\n')
        body.append("Model: ").append(Build.MODEL).append('\n')
        body.append("Resolution: ")
                .append(dm.heightPixels)
                .append("x")
                .append(dm.widthPixels)
                .append('\n')
        body.append("Density: ")
                .append(dm.densityDpi)
                .append("dpi (")
                .append(densityBucket)
                .append(")\n")
        body.append("Release: ").append(Build.VERSION.RELEASE).append('\n')
        body.append("API: ").append(Build.VERSION.SDK_INT).append('\n')
        body.append("{panel}")

        val txt = ArrayList<String>()
        txt.add(body.toString())

        if (screenshot != null && report.includeScreenshot) {
            intent.addStream(Uri.fromFile(screenshot))
        }
        if (logs != null) {
            intent.addStream(Uri.fromFile(logs))
        }
        val chooser = intent.createChooserIntent()
        chooser.putStringArrayListExtra(Intent.EXTRA_TEXT, txt)
        context.startActivity(chooser)
    }

    private fun getDensityString(displayMetrics: DisplayMetrics): String {
        when (displayMetrics.densityDpi) {
            DisplayMetrics.DENSITY_LOW -> return "ldpi"
            DisplayMetrics.DENSITY_MEDIUM -> return "mdpi"
            DisplayMetrics.DENSITY_HIGH -> return "hdpi"
            DisplayMetrics.DENSITY_XHIGH -> return "xhdpi"
            DisplayMetrics.DENSITY_XXHIGH -> return "xxhdpi"
            DisplayMetrics.DENSITY_XXXHIGH -> return "xxxhdpi"
            DisplayMetrics.DENSITY_TV -> return "tvdpi"
            else -> return displayMetrics.densityDpi.toString()
        }
    }
}
