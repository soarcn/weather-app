package com.jakewharton.u2020.ui.debug

import android.view.View
import android.view.ViewGroup

/**
 * A [hierarchy change listener][android.view.ViewGroup.OnHierarchyChangeListener] which recursively
 * monitors an entire tree of views.
 */
class HierarchyTreeChangeListener private constructor(private val delegate: ViewGroup.OnHierarchyChangeListener) : ViewGroup.OnHierarchyChangeListener {


    override fun onChildViewAdded(parent: View, child: View) {
        delegate.onChildViewAdded(parent, child)

        if (child is ViewGroup) {
            val childGroup = child
            childGroup.setOnHierarchyChangeListener(this)
            for (i in 0 until childGroup.childCount) {
                onChildViewAdded(childGroup, childGroup.getChildAt(i))
            }
        }
    }

    override fun onChildViewRemoved(parent: View, child: View) {
        if (child is ViewGroup) {
            val childGroup = child
            for (i in 0 until childGroup.childCount) {
                onChildViewRemoved(childGroup, childGroup.getChildAt(i))
            }
            childGroup.setOnHierarchyChangeListener(null)
        }

        delegate.onChildViewRemoved(parent, child)
    }

    companion object {
        /**
         * Wrap a regular [hierarchy change listener][android.view.ViewGroup.OnHierarchyChangeListener] with one
         * that monitors an entire tree of views.
         */
        fun wrap(delegate: ViewGroup.OnHierarchyChangeListener): HierarchyTreeChangeListener {
            return HierarchyTreeChangeListener(delegate)
        }
    }
}
