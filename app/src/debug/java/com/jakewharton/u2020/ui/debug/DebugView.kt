package com.jakewharton.u2020.ui.debug

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.ContextWrapper
import android.os.Build
import android.os.SystemClock.sleep
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ContextThemeWrapper
import com.blankj.utilcode.util.KeyboardUtils
import com.cocosw.weather.R
import com.cocosw.weather.base.LumberYard
import com.cocosw.weather.base.NAME
import com.cocosw.weather.base.NAME.API
import com.f2prateek.rx.preferences2.Preference
import com.jakewharton.processphoenix.ProcessPhoenix
import com.jakewharton.rxbinding2.widget.RxAdapterView
import com.jakewharton.u2020.data.ApiEndpoints
import com.jakewharton.u2020.data.NetworkErrorCode
import com.jakewharton.u2020.data.prefs.InetSocketAddressPreferenceAdapter
import com.jakewharton.u2020.ui.logs.LogsDialog
import com.jakewharton.u2020.ui.misc.EnumAdapter
import okhttp3.OkHttpClient
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.mock.NetworkBehavior
import timber.log.Timber
import java.net.InetSocketAddress
import java.util.concurrent.TimeUnit.MILLISECONDS

class DebugView constructor(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs), KoinComponent {

    lateinit var contextualTitleView: View
    lateinit var contextualListView: LinearLayout

    lateinit var endpointView: Spinner
    lateinit var endpointEditView: View
    lateinit var networkDelayView: Spinner
    lateinit var networkVarianceView: Spinner
    lateinit var networkFailureView: Spinner
    lateinit var networkErrorView: Spinner
    lateinit var networkErrorCodeView: Spinner
    lateinit var networkProxyView: Spinner
    lateinit var networkLoggingView: Spinner

    //    @BindView(R.id.debug_ui_animation_speed)
    lateinit var uiAnimationSpeedView: Spinner
    //    @BindView(R.id.debug_ui_pixel_grid)
    lateinit var uiPixelGridView: Switch
    //    @BindView(R.id.debug_ui_pixel_ratio)
    lateinit var uiPixelRatioView: Switch
    //    @BindView(R.id.debug_ui_scalpel)
    lateinit var uiScalpelView: Switch
    //    @BindView(R.id.debug_ui_scalpel_wireframe)
    lateinit var uiScalpelWireframeView: Switch

    lateinit var deviceMakeView: TextView
    lateinit var deviceModelView: TextView
    lateinit var deviceResolutionView: TextView
    lateinit var deviceDensityView: TextView
    lateinit var deviceReleaseView: TextView
    lateinit var deviceApiView: TextView

    val client: OkHttpClient by inject("default")
    val apiClient: OkHttpClient by inject(API)
    val lumberYard: LumberYard by inject()
    val isMockMode: Boolean by inject(NAME.IS_MOCK_MODE)
    val networkEndpoint: Preference<String> by inject("ApiEndpoint")
    val networkProxyAddress: Preference<InetSocketAddress> by inject("InetSocketAddress")
    lateinit var captureIntents: Preference<Boolean>
    val animationSpeed: Preference<Int> by inject("AnimationSpeed")
    val pixelGridEnabled: Preference<Boolean> by inject("PixelGridEnabled")
    val pixelRatioEnabled: Preference<Boolean> by inject("PixelRatioEnabled")
    val scalpelEnabled: Preference<Boolean> by inject("ScalpelEnabled")
    val scalpelWireframeEnabled: Preference<Boolean> by inject("ScalpelWireframeEnabled")
    val behavior: NetworkBehavior by inject()
    val networkDelay: Preference<Long> by inject("NetworkDelay")
    val networkVariancePercent: Preference<Int> by inject("NetworkVariancePercent")
    val networkFailurePercent: Preference<Int> by inject("NetworkFailurePercent")
    val networkErrorPercent: Preference<Int> by inject("NetworkErrorPercent")
    val networkErrorCode: Preference<NetworkErrorCode>  by inject("NetworkErrorCode")
    val app: Application by inject()

    init {
        LayoutInflater.from(context).inflate(R.layout.debug_view_content, this)
        setupUI()
        setupNetworkSection()
        setupUserInterfaceSection()
        setupDeviceSection()
    }


    private fun setupUI() {
        contextualTitleView = findViewById(R.id.debug_contextual_title)
        contextualListView = findViewById(R.id.debug_contextual_list)
        endpointView = findViewById(R.id.debug_network_endpoint)
        endpointEditView = findViewById(R.id.debug_network_endpoint_edit)
        endpointEditView.setOnClickListener { onEditEndpointClicked() }
        networkDelayView = findViewById(R.id.debug_network_delay)
        networkVarianceView = findViewById(R.id.debug_network_variance)
        networkFailureView = findViewById(R.id.debug_network_failure)
        networkErrorView = findViewById(R.id.debug_network_error)
        networkErrorCodeView = findViewById(R.id.debug_network_error_code)
        networkProxyView = findViewById(R.id.debug_network_proxy)
        networkLoggingView = findViewById(R.id.debug_network_logging)

        deviceMakeView = findViewById(R.id.debug_device_make)
        deviceModelView = findViewById(R.id.debug_device_model)
        deviceResolutionView = findViewById(R.id.debug_device_resolution)
        deviceDensityView = findViewById(R.id.debug_device_density)
        deviceReleaseView = findViewById(R.id.debug_device_release)
        deviceApiView = findViewById(R.id.debug_device_api)

        uiAnimationSpeedView = findViewById(R.id.debug_ui_animation_speed)
        uiPixelGridView = findViewById(R.id.debug_ui_pixel_grid)
        uiPixelRatioView = findViewById(R.id.debug_ui_pixel_ratio)
        uiScalpelView = findViewById(R.id.debug_ui_scalpel)
        uiScalpelWireframeView = findViewById(R.id.debug_ui_scalpel_wireframe)

        findViewById<View>(R.id.debug_logs_show).setOnClickListener { showLogs() }
        findViewById<View>(R.id.mock_setting).apply {
            isEnabled = isMockMode
            setOnClickListener {
                (context.toActivity() as AppCompatActivity).supportFragmentManager.apply {
                    val transaction = beginTransaction()
                    val TAG = "DEBUG"
                    var instructionFragment = findFragmentByTag(TAG)
                    if (instructionFragment != null) {
                        transaction.remove(instructionFragment)
                    }
                    instructionFragment = MockSettingDialog()
                    transaction.addToBackStack(null)
                    instructionFragment.show(this, TAG)
                }
            }
        }
    }

    @SuppressLint("CheckResult")
    private fun setupNetworkSection() {
        val currentEndpoint = ApiEndpoints.from(networkEndpoint.get())
        val endpointAdapter = EnumAdapter(context, ApiEndpoints::class.java)
        endpointView.adapter = endpointAdapter
        endpointView.setSelection(currentEndpoint.ordinal)

        RxAdapterView.itemSelections<SpinnerAdapter>(endpointView)
                .map { endpointAdapter.getItem(it) }
                .filter { item -> item !== currentEndpoint }
                .subscribe { selected ->
                    if (selected === ApiEndpoints.CUSTOM) {
                        Timber.d("Custom network endpoint selected. Prompting for URL.")
                        showCustomEndpointDialog(currentEndpoint.ordinal, "http://")
                    } else {
                        setEndpointAndRelaunch(selected?.url.toString())
                    }
                }

        val delayAdapter = NetworkDelayAdapter(context)
        networkDelayView.adapter = delayAdapter
        networkDelayView.setSelection(
                NetworkDelayAdapter.getPositionForValue(behavior.delay(MILLISECONDS)))

        RxAdapterView.itemSelections<SpinnerAdapter>(networkDelayView)
                .map { delayAdapter.getItem(it) }
                .filter { item -> item != behavior.delay(MILLISECONDS) }
                .subscribe { selected ->
                    Timber.d("Setting network delay to %sms", selected)
                    behavior.setDelay(selected, MILLISECONDS)
                    networkDelay.set(selected)
                }

        val varianceAdapter = NetworkVarianceAdapter(context)
        networkVarianceView.adapter = varianceAdapter
        networkVarianceView.setSelection(
                NetworkVarianceAdapter.getPositionForValue(behavior.variancePercent()))

        RxAdapterView.itemSelections<SpinnerAdapter>(networkVarianceView)
                .map { varianceAdapter.getItem(it) }
                .filter { item -> item != behavior.variancePercent() }
                .subscribe { selected ->
                    Timber.d("Setting network variance to %s%%", selected)
                    behavior.setVariancePercent(selected)
                    networkVariancePercent.set(selected)
                }

        val failureAdapter = NetworkPercentageAdapter(context)
        networkFailureView.adapter = failureAdapter
        networkFailureView.setSelection(
                NetworkPercentageAdapter.getPositionForValue(behavior.failurePercent()))

        RxAdapterView.itemSelections<SpinnerAdapter>(networkFailureView)
                .map { failureAdapter.getItem(it) }
                .filter { item -> item != behavior.failurePercent() }
                .subscribe { selected ->
                    Timber.d("Setting network failure to %s%%", selected)
                    behavior.setFailurePercent(selected)
                    networkFailurePercent.set(selected)
                }

        val errorAdapter = NetworkPercentageAdapter(context)
        networkErrorView.adapter = errorAdapter
        networkErrorView.setSelection(
                NetworkPercentageAdapter.getPositionForValue(behavior.errorPercent()))

        RxAdapterView.itemSelections<SpinnerAdapter>(networkErrorView)
                .map { errorAdapter.getItem(it) }
                .filter { item -> item != behavior.errorPercent() }
                .subscribe { selected ->
                    Timber.d("Setting network error to %s%%", selected)
                    behavior.setErrorPercent(selected)
                    networkErrorPercent.set(selected)
                }

        val errorCodeAdapter = EnumAdapter(context, NetworkErrorCode::class.java)
        networkErrorCodeView.adapter = errorCodeAdapter
        networkErrorCodeView.setSelection(networkErrorCode.get().ordinal)

        RxAdapterView.itemSelections<SpinnerAdapter>(networkErrorCodeView)
                .map { errorCodeAdapter.getItem(it) }
                .filter { item -> item !== networkErrorCode.get() }
                .subscribe { selected ->
                    Timber.d("Setting network error code to %s%%", selected)
                    selected?.let { networkErrorCode.set(it) }
                    // No need to update 'behavior' as the factory already pulls from the preference.
                }

        val currentProxyPosition = if (networkProxyAddress.isSet) ProxyAdapter.PROXY else ProxyAdapter.NONE
        val proxyAdapter = ProxyAdapter(context, networkProxyAddress)
        networkProxyView.adapter = proxyAdapter
        networkProxyView.setSelection(currentProxyPosition)

        RxAdapterView.itemSelections<SpinnerAdapter>(networkProxyView)
                .filter { position -> !networkProxyAddress.isSet || position != ProxyAdapter.PROXY }
                .subscribe { position ->
                    if (position == ProxyAdapter.NONE) {
                        // Only clear the proxy and restart if one was previously set.
                        if (currentProxyPosition != ProxyAdapter.NONE) {
                            Timber.d("Clearing network proxy")
                            // TODO: Keep the custom proxy around so you can easily switch back and forth.
                            networkProxyAddress.delete()
                            // Force a restart to re-initialize the app without a proxy.
                            ProcessPhoenix.triggerRebirth(context)
                        }
                    } else if (networkProxyAddress.isSet && position == ProxyAdapter.PROXY) {
                        Timber.d("Ignoring re-selection of network proxy %s", networkProxyAddress.get())
                    } else {
                        Timber.d("New network proxy selected. Prompting for host.")
                        showNewNetworkProxyDialog()
                    }
                }

        // Only show the endpoint editor when a custom endpoint is in use.
        endpointEditView.visibility = if (currentEndpoint === ApiEndpoints.CUSTOM) View.VISIBLE else View.GONE

        if (currentEndpoint === ApiEndpoints.MOCK_MODE) {
            // Disable network proxy if we are in mock mode.
            networkProxyView.isEnabled = false
            networkLoggingView.isEnabled = false
        } else {
            // Disable network controls if we are not in mock mode.
            networkDelayView.isEnabled = false
            networkVarianceView.isEnabled = false
            networkFailureView.isEnabled = false
            networkErrorView.isEnabled = false
            networkErrorCodeView.isEnabled = false
        }

        // We use the JSON rest adapter as the source of truth for the log level.
        //final EnumAdapter<RestAdapter.LogLevel> loggingAdapter =
        //    new EnumAdapter<>(getContext(), RestAdapter.LogLevel.class);
        //networkLoggingView.setAdapter(loggingAdapter);
        //networkLoggingView.setSelection(retrofit.getLogLevel().ordinal());
        //networkLoggingView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        //  @Override
        //  public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        //    RestAdapter.LogLevel selected = loggingAdapter.getItem(position);
        //    if (selected != retrofit.getLogLevel()) {
        //      Timber.d("Setting logging level to %s", selected);
        //      retrofit.setLogLevel(selected);
        //    } else {
        //      Timber.d("Ignoring re-selection of logging level " + selected);
        //    }
        //  }
        //
        //  @Override public void onNothingSelected(AdapterView<?> adapterView) {
        //  }
        //});
    }

    internal fun onEditEndpointClicked() {
        Timber.d("Prompting to edit custom endpoint URL.")
        // Pass in the currently selected position since we are merely editing.
        showCustomEndpointDialog(endpointView.selectedItemPosition, networkEndpoint.get())
    }


    private fun setupUserInterfaceSection() {
        val speedAdapter = AnimationSpeedAdapter(context)
        uiAnimationSpeedView.adapter = speedAdapter
        val animationSpeedValue = animationSpeed.get()
        uiAnimationSpeedView.setSelection(
                AnimationSpeedAdapter.getPositionForValue(animationSpeedValue))

        RxAdapterView.itemSelections<SpinnerAdapter>(uiAnimationSpeedView)
                .map { speedAdapter.getItem(it) }
                .filter { item -> item != animationSpeed.get() }
                .subscribe { selected ->
                    Timber.d("Setting animation speed to %sx", selected)
                    animationSpeed.set(selected)
                    applyAnimationSpeed(selected)
                }
        // Ensure the animation speed value is always applied across app restarts.
        post { applyAnimationSpeed(animationSpeedValue) }

        val gridEnabled = pixelGridEnabled.get()
        uiPixelGridView.isChecked = gridEnabled
        uiPixelRatioView.isEnabled = gridEnabled
        uiPixelGridView.setOnCheckedChangeListener { _, isChecked ->
            Timber.d("Setting pixel grid overlay enabled to %b", isChecked)
            pixelGridEnabled.set(isChecked)
            uiPixelRatioView.isEnabled = isChecked
        }

        uiPixelRatioView.isChecked = pixelRatioEnabled.get()
        uiPixelRatioView.setOnCheckedChangeListener { _, isChecked ->
            Timber.d("Setting pixel scale overlay enabled to %b", isChecked)
            pixelRatioEnabled.set(isChecked)
        }

        uiScalpelView.isChecked = scalpelEnabled.get()
        uiScalpelWireframeView.isEnabled = scalpelEnabled.get()
        uiScalpelView.setOnCheckedChangeListener { _, isChecked ->
            Timber.d("Setting scalpel interaction enabled to %b", isChecked)
            scalpelEnabled.set(isChecked)
            uiScalpelWireframeView.isEnabled = isChecked
        }

        uiScalpelWireframeView.isChecked = scalpelWireframeEnabled.get()
        uiScalpelWireframeView.setOnCheckedChangeListener { _, isChecked ->
            Timber.d("Setting scalpel wireframe enabled to %b", isChecked)
            scalpelWireframeEnabled.set(isChecked)
        }
    }

    internal fun showLogs() {
        LogsDialog(ContextThemeWrapper(context, R.style.AppTheme), lumberYard).show()
    }

    private fun setupDeviceSection() {
        val displayMetrics = context.resources.displayMetrics
        val densityBucket = getDensityString(displayMetrics)
        deviceResolutionView.text = displayMetrics.heightPixels.toString() + "x" + displayMetrics.widthPixels
        deviceDensityView.text = displayMetrics.densityDpi.toString() + "dpi (" + densityBucket + ")"
        deviceReleaseView.text = Build.VERSION.RELEASE
        deviceApiView.text = Build.VERSION.SDK_INT.toString()
    }


    private fun applyAnimationSpeed(multiplier: Int) {
        try {
            val method = ValueAnimator::class.java.getDeclaredMethod("setDurationScale", Float::class.javaPrimitiveType)
            method.invoke(null, multiplier.toFloat())
        } catch (e: Exception) {
            throw RuntimeException("Unable to apply animation speed.", e)
        }
    }

    private fun showNewNetworkProxyDialog() {
        val originalSelection = if (networkProxyAddress.isSet) ProxyAdapter.PROXY else ProxyAdapter.NONE

        val view = LayoutInflater.from(app).inflate(R.layout.debug_drawer_network_proxy, this, false)
        val hostView = view.findViewById<EditText>(R.id.debug_drawer_network_proxy_host)

        if (networkProxyAddress.isSet) {
            val host = networkProxyAddress.get().hostName
            hostView.setText(host) // Set the current host.
            hostView.setSelection(0, host.length) // Pre-select it for editing.

            // Show the keyboard. Post this to the next frame when the dialog has been attached.
            hostView.post { KeyboardUtils.showSoftInput(hostView) }
        }

        AlertDialog.Builder(context) //
                .setTitle("Set Network Proxy")
                .setView(view)
                .setNegativeButton("Cancel") { dialog, _ ->
                    networkProxyView.setSelection(originalSelection)
                    dialog.cancel()
                }
                .setPositiveButton("Use") { _, _ ->
                    val `in` = hostView.text.toString()
                    val address = InetSocketAddressPreferenceAdapter.parse(`in`)
                    if (address != null) {
                        networkProxyAddress.set(address)
                        // Force a restart to re-initialize the app with the new proxy.
                        ProcessPhoenix.triggerRebirth(context)
                    } else {
                        networkProxyView.setSelection(originalSelection)
                    }
                }
                .setOnCancelListener { networkProxyView.setSelection(originalSelection) }
                .show()
    }

    private fun showCustomEndpointDialog(originalSelection: Int, defaultUrl: String) {
        val view = LayoutInflater.from(app).inflate(R.layout.debug_drawer_network_endpoint, null)
        val url = view.findViewById<EditText>(R.id.debug_drawer_network_endpoint_url)
        url.setText(defaultUrl)
        url.setSelection(url.length())

        AlertDialog.Builder(context) //
                .setTitle("Set Network Endpoint")
                .setView(view)
                .setNegativeButton("Cancel") { dialog, _ ->
                    endpointView.setSelection(originalSelection)
                    dialog.cancel()
                }
                .setPositiveButton("Use") { _, _ ->
                    val theUrl = url.text.toString()
                    if (!theUrl.isEmpty()) {
                        setEndpointAndRelaunch(theUrl)
                    } else {
                        endpointView.setSelection(originalSelection)
                    }
                }
                .setOnCancelListener { endpointView.setSelection(originalSelection) }
                .show()
    }

    private fun setEndpointAndRelaunch(endpoint: String?) {
        Timber.d("Setting network endpoint to %s", endpoint)
        endpoint?.let { networkEndpoint.set(it) }
        sleep(2000)
        ProcessPhoenix.triggerRebirth(context)
    }


    //private val DATE_DISPLAY_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm a", Locale.US).withZone(ZoneId.systemDefault())
    private fun getDensityString(displayMetrics: DisplayMetrics): String {
        return when (displayMetrics.densityDpi) {
            DisplayMetrics.DENSITY_LOW -> "ldpi"
            DisplayMetrics.DENSITY_MEDIUM -> "mdpi"
            DisplayMetrics.DENSITY_HIGH -> "hdpi"
            DisplayMetrics.DENSITY_XHIGH -> "xhdpi"
            DisplayMetrics.DENSITY_XXHIGH -> "xxhdpi"
            DisplayMetrics.DENSITY_XXXHIGH -> "xxxhdpi"
            DisplayMetrics.DENSITY_TV -> "tvdpi"
            else -> displayMetrics.densityDpi.toString()
        }
    }

}

fun Context.toActivity(): Activity? {
    if (this is ContextWrapper) {
        return this as? Activity ?: this.baseContext.toActivity()
    }
    return null
}
