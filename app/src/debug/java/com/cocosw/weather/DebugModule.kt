package com.cocosw.weather

import android.app.Application
import android.content.SharedPreferences
import androidx.fragment.app.Fragment
import com.cocosw.weather.base.LumberYard
import com.cocosw.weather.base.NAME
import com.cocosw.weather.base.NAME.IS_MOCK_MODE
import com.cocosw.weather.service.LocationProvider
import com.cocosw.weather.service.WeatherApi
import com.cocosw.weather.servicemock.MockLocationProvider
import com.cocosw.weather.servicemock.MockResponseSupplier
import com.cocosw.weather.servicemock.MockWeatherApi
import com.cocosw.weather.ui.LocusLocationProvider
import com.f2prateek.rx.preferences2.Preference
import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.jakewharton.u2020.data.ApiEndpoints
import com.jakewharton.u2020.data.NetworkErrorCode
import com.jakewharton.u2020.data.SharedPreferencesMockResponseSupplier
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.Module
import org.koin.dsl.module.module
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior
import timber.log.Timber
import java.net.InetSocketAddress
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


/**
 * Api dependencies
 */

val debugApiModule: Module = module(override = true) {
    single(NAME.FRAMELAYOUT) { R.layout.state_activity }
    single { provideHttpUrl(get("ApiEndpoint")) }
    single { provideLoggingInterceptor() }
    single("Api") { provideApiClient(get("default"), get()) }
    single { provideMockRetrofit(get(), get()) }
    single { provideMockApi(get(), get()) }
    single { provideApiService(get(), get(IS_MOCK_MODE), get()) }
    single { provideMockLocationProvider(get(), get()) }
    factory { (fragment: Fragment) -> provideMockLocation(get(IS_MOCK_MODE), fragment, get()) as LocationProvider }
    single { provideLumberYard(androidApplication()) }
    single {provideResponseSupplier(get())}
    single { provideBehavior(get("NetworkDelay"), get("NetworkVariancePercent"), get("NetworkFailurePercent"), get("NetworkErrorPercent"), get("NetworkErrorCode")) }
}

private fun provideMockLocationProvider(
    mockRetrofit: MockRetrofit,
    responseSupplier: MockResponseSupplier
): MockLocationProvider {
    return MockLocationProvider(mockRetrofit, responseSupplier)
}

private fun provideMockLocation(isMockMode: Boolean, fragment: Fragment, mock: MockLocationProvider): LocationProvider {
    return if (isMockMode) mock else LocusLocationProvider(fragment)
}

private fun provideResponseSupplier(preferences: SharedPreferences): MockResponseSupplier {
    return SharedPreferencesMockResponseSupplier(preferences)
}

private fun provideLumberYard(androidApplication: Application): LumberYard {
    val yard = LumberYard(androidApplication)
    yard.cleanUp()
    Timber.plant(yard.tree())
    return yard
}

private fun provideMockApi(mockRetrofit: MockRetrofit,
             responseSupplier: MockResponseSupplier
): MockWeatherApi {
    return MockWeatherApi(mockRetrofit, responseSupplier)
}

private fun provideApiService(retrofit: Retrofit,
                               isMockMode: Boolean, mockService: MockWeatherApi): WeatherApi {
    return if (isMockMode) mockService else retrofit.create(WeatherApi::class.java)
}

private fun provideHttpUrl(apiEndpoint: Preference<String>): HttpUrl {
    return HttpUrl.parse(apiEndpoint.get())!!
}

private fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    val loggingInterceptor = HttpLoggingInterceptor { message -> Timber.tag("OkHttp").v(message) }
    loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    return loggingInterceptor
}

private fun provideApiClient(client: OkHttpClient,
                             loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
    return createApiClient(client)
            .addInterceptor(loggingInterceptor)
            .build()
}


internal fun provideBehavior(networkDelay: Preference<Long>,
                             networkVariancePercent: Preference<Int>,
                             networkFailurePercent: Preference<Int>,
                             networkErrorPercent: Preference<Int>,
                             networkErrorCode: Preference<NetworkErrorCode>): NetworkBehavior {
    val behavior = NetworkBehavior.create()
    behavior.setDelay(networkDelay.get(), TimeUnit.MILLISECONDS)
    behavior.setVariancePercent(networkVariancePercent.get())
    behavior.setFailurePercent(networkFailurePercent.get())
    behavior.setErrorPercent(networkErrorPercent.get())
    behavior.setErrorFactory { Response.error<Any>(networkErrorCode.get().code, ResponseBody.create(null, ByteArray(0))) }
    return behavior
}

private fun provideMockRetrofit(retrofit: Retrofit,
                                behavior: NetworkBehavior): MockRetrofit {
    return MockRetrofit.Builder(retrofit)
            .networkBehavior(behavior)
            .build()
}


//<------------------------------------------------------------>


val debugNetworkModule: Module = module(override = true) {
    single("default") { provideOkHttpClient(this.androidApplication(), get("InetSocketAddress")) }
}


private fun provideOkHttpClient(app: Application,
                                networkProxyAddress: Preference<InetSocketAddress>): OkHttpClient {
    return createOkHttpClient(app)
            .sslSocketFactory(createBadSslSocketFactory())
            //.proxy(InetSocketAddressPreferenceAdapter.createProxy(networkProxyAddress.get()))
            .build()
}

private fun createBadSslSocketFactory(): SSLSocketFactory {
    try {
        // Construct SSLSocketFactory that accepts any cert.
        val context = SSLContext.getInstance("TLS")
        val permissive = object : X509TrustManager {
            @Throws(CertificateException::class)
            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
            }

            @Throws(CertificateException::class)
            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
            }

            override fun getAcceptedIssuers(): Array<X509Certificate?> {
                return arrayOfNulls(0)
            }
        }
        context.init(null, arrayOf<TrustManager>(permissive), null)
        return context.socketFactory
    } catch (e: Exception) {
        throw AssertionError(e)
    }
}

//<------------------------------------------------------------>

val debugDataModule: Module = module(override = true) {
    single("ApiEndpoint") { provideEndpointPreference(get()) }
    single("IsMockMode") { provideIsMockMode(get("ApiEndpoint"), get("IsInstrumentationTest")) }
    single("NetworkDelay") { provideNetworkDelay(get()) }
    single("NetworkFailurePercent") { provideNetworkFailurePercent(get()) }
    single("NetworkErrorPercent") { provideNetworkErrorPercent(get()) }
    single("NetworkErrorCode") { provideNetworkErrorCode(get()) }
    single("NetworkVariancePercent") { provideNetworkVariancePercent(get()) }
    single("InetSocketAddress") { provideNetworkProxyAddress(get()) }
    single("AnimationSpeed") { provideAnimationSpeed(get()) }
    single("PicassoDebugging") { providePicassoDebugging(get()) }
    single("PixelGridEnabled") { providePixelGridEnabled(get()) }
    single("PixelRatioEnabled") { providePixelRatioEnabled(get()) }
    single("SeenDebugDrawer") { provideSeenDebugDrawer(get()) }
    single("ScalpelEnabled") { provideScalpelEnabled(get()) }
    single("ScalpelWireframeEnabled") { provideScalpelWireframeEnabled(get()) }
}

private fun provideEndpointPreference(preferences: RxSharedPreferences): Preference<String> {
    return preferences.getString("debug_endpoint", ApiEndpoints.MOCK_MODE.url)
}

private fun provideIsMockMode(endpoint: Preference<String>, isInstrumentationTest: Boolean): Boolean {
    // Running in an instrumentation forces mock mode.
    return isInstrumentationTest || ApiEndpoints.isMockMode(endpoint.get())
}

private fun provideNetworkDelay(preferences: RxSharedPreferences): Preference<Long> {
    return preferences.getLong("debug_network_delay", 2000L)
}

private fun provideNetworkFailurePercent(preferences: RxSharedPreferences): Preference<Int> {
    return preferences.getInteger("debug_network_failure_percent", 3)
}

private fun provideNetworkErrorPercent(preferences: RxSharedPreferences): Preference<Int> {
    return preferences.getInteger("debug_network_error_percent", 0)
}

private fun provideNetworkErrorCode(preferences: RxSharedPreferences): Preference<NetworkErrorCode> {
    return preferences.getEnum("debug_network_error_code", NetworkErrorCode.HTTP_500,
            NetworkErrorCode::class.java)
}

private fun provideNetworkVariancePercent(preferences: RxSharedPreferences): Preference<Int> {
    return preferences.getInteger("debug_network_variance_percent", 40)
}

private fun provideNetworkProxyAddress(preferences: RxSharedPreferences): Preference<InetSocketAddress> {
    return preferences.getObject("debug_network_proxy", InetSocketAddress(80),
            com.jakewharton.u2020.data.prefs.InetSocketAddressPreferenceAdapter.INSTANCE)
}

private fun provideAnimationSpeed(preferences: RxSharedPreferences): Preference<Int> {
    return preferences.getInteger("debug_animation_speed", DEFAULT_ANIMATION_SPEED)
}

private fun providePicassoDebugging(preferences: RxSharedPreferences): Preference<Boolean> {
    return preferences.getBoolean("debug_picasso_debugging", DEFAULT_PICASSO_DEBUGGING)
}

private fun providePixelGridEnabled(preferences: RxSharedPreferences): Preference<Boolean> {
    return preferences.getBoolean("debug_pixel_grid_enabled", DEFAULT_PIXEL_GRID_ENABLED)
}

private fun providePixelRatioEnabled(preferences: RxSharedPreferences): Preference<Boolean> {
    return preferences.getBoolean("debug_pixel_ratio_enabled", DEFAULT_PIXEL_RATIO_ENABLED)
}

private fun provideSeenDebugDrawer(preferences: RxSharedPreferences): Preference<Boolean> {
    return preferences.getBoolean("debug_seen_debug_drawer", DEFAULT_SEEN_DEBUG_DRAWER)
}

private fun provideScalpelEnabled(preferences: RxSharedPreferences): Preference<Boolean> {
    return preferences.getBoolean("debug_scalpel_enabled", DEFAULT_SCALPEL_ENABLED)
}

private fun provideScalpelWireframeEnabled(preferences: RxSharedPreferences): Preference<Boolean> {
    return preferences.getBoolean("debug_scalpel_wireframe_drawer",
            DEFAULT_SCALPEL_WIREFRAME_ENABLED)
}

private val DEFAULT_ANIMATION_SPEED = 1 // 1x (normal) speed.
private val DEFAULT_PICASSO_DEBUGGING = false // Debug indicators displayed
private val DEFAULT_PIXEL_GRID_ENABLED = false // No pixel grid overlay.
private val DEFAULT_PIXEL_RATIO_ENABLED = false // No pixel ratio overlay.
private val DEFAULT_SCALPEL_ENABLED = false // No crazy 3D view tree.
private val DEFAULT_SCALPEL_WIREFRAME_ENABLED = false // Draw views by default.
private val DEFAULT_SEEN_DEBUG_DRAWER = false // Show debug drawer first time.

