package com.cocosw.weather

import android.app.Application
import com.cocosw.weather.ui.weatherModule
import org.koin.dsl.module.Module
import timber.log.Timber


object Injector {

    fun init(app: Application) {
    }

    fun module(): List<Module> {
        Timber.plant(Timber.DebugTree())
        return listOf(
            appModule, networkModule, apiModule, weatherModule,
                // development related module, don't forget put the module above to prod release Injector
                debugApiModule, debugDataModule, debugNetworkModule)
    }
}
