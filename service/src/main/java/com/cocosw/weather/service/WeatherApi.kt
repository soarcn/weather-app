package com.cocosw.weather.service

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("weather")
    fun getWeatherByCity(
        @Query("q") city: String
    ): Single<WeatherInfo>

    @GET("weather")
    fun getWeatherByZipcode(
        @Query("zip") zip: String
    ): Single<WeatherInfo>

    @GET("weather")
    fun getWeatherByGps(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double
    ): Single<WeatherInfo>
}