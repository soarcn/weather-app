package com.cocosw.weather.service

import io.reactivex.Single

interface LocationProvider {
    fun getLocation(): Single<Pair<Double, Double>>
}